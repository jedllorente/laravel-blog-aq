@extends('layouts.app')

@section('content')
<h1>{{$post->title}}</h1>
<hr>
<small>Written on: {{$post->created_at}}</small>
<p>{{$post->body}}</p>
<hr>
<td>
    <!-- Spoofing -->
    <form method="POST" action="/posts/{{$post->id}}">
        @csrf
        <input type="hidden" name="_method" value="DELETE">
        <button class="btn btn-danger">DELETE</button>
    </form>
</td>
<hr>

@endsection