<h1>Hello Cherry Poppersmith!</h1>

<h2>{{ $front_end }}</h2>

@if (count($topics) > 0)
@foreach($topics as $topic)
<li>{{ $topic }}</li>
@endforeach
@else
<h2>nothing to display</h2>
@endif